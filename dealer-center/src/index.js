class WrongDataTypeError extends Error {
  constructor(message) {
    super(message);

    this.name = 'WrongDataTypeError';
  }
}

class IsNotInstanceOfVehicleError extends Error {
  constructor(message) {
    super(message);

    this.name = 'IsNotInstanceOfVehicleError';
  }
}

class EntityFoundError extends Error {
  constructor(message) {
    super(message);

    this.name = 'EntityFoundError';
  }
}

class NotFoundError extends Error {
  constructor(message) {
    super(message);

    this.name = 'NotFoundError';
  }
}

const DELAY = 1500;

const DATABASE = {
  dealer: {
    title: 'Trucks & Buses Vilnius LTD',
  },
  trucks: [
    {
      vin: 1112,
      color: 'Red',
      carryWeight: 10,
    },
    {
      vin: 2332,
      color: 'Yellow',
      carryWeight: 20,
    },
    {
      vin: 5234,
      color: 'Green',
      carryWeight: 70,
    },
  ],
  buses: [
    {
      vin: 1112,
      color: 'Green',
      maxPassengers: 50,
    },
    {
      vin: 6543,
      color: 'Yellow',
      maxPassengers: 25,
    },
  ],
};

class Vehicle {
  #vin = 0;

  #color = '';

  constructor(vin, color) {
    this.vin = vin;
    this.color = color;
  }

  set vin(newVin) {
    this.dataVerification({ vin: newVin });

    this.#vin = newVin;
  }

  get vin() {
    return this.#vin;
  }

  set color(newColor) {
    this.dataVerification({ color: newColor });

    this.#color = newColor;
  }

  get color() {
    return this.#color;
  }

  dataVerification(data) {
    Object.keys(data).forEach((property) => {
      switch (property) {
        case 'color':
          if (typeof data.color !== 'string' || data.color === '') {
            throw new WrongDataTypeError(
              `Color should be a not empty string! Check the entered data: ${data.color.toLowerCase()}`
            );
          }
          break;

        case 'vin':
          if (typeof data.vin !== 'number') {
            throw new WrongDataTypeError(`Vin should be a number! Check the entered data: ${data.vin}`);
          }
          break;

        case 'maxPassengers':
          if (typeof data.maxPassengers !== 'number' || data.carryWeight <= 0) {
            throw new WrongDataTypeError(
              `Max passengers should be a positive number! Check the entered data: ${data.maxPassengers}`
            );
          }
          break;

        case 'carryWeight':
          if (typeof data.carryWeight !== 'number' || data.carryWeight <= 0) {
            throw new WrongDataTypeError(
              `Carry weight should be a string! Check the entered data: ${data.carryWeight}`
            );
          }
          break;

        default:
          break;
      }
    });
  }
}

class Bus extends Vehicle {
  #maxPassengers = 0;

  constructor(vin, color, maxPassengers) {
    super(vin, color);
    this.maxPassengers = maxPassengers;
  }

  set maxPassengers(newMaxPassengers) {
    this.dataVerification({ maxPassengers: newMaxPassengers });
    this.#maxPassengers = newMaxPassengers;
  }

  get maxPassengers() {
    return this.#maxPassengers;
  }
}

class Dealer {
  #title = '';

  #vehicles = [];

  constructor(title) {
    this.title = title;
  }

  get vehicles() {
    return this.#vehicles;
  }

  set title(newTitle) {
    this.dataVerification({ title: newTitle });

    this.#title = newTitle;
  }

  get title() {
    return this.#title;
  }

  dataVerification(data) {
    Object.keys(data).forEach((property) => {
      switch (property) {
        case 'title':
          if (typeof data.title !== 'string' || data.title === '') {
            throw new WrongDataTypeError(`Title should be a not empty string! Check the entered data: ${data.title}`);
          }
          break;

        case 'carryWeight':
          if (typeof data.carryWeight !== 'number' || data.carryWeight <= 0) {
            throw new WrongDataTypeError(
              `Carry weight should be a positive number! Check the entered data: ${data.carryWeight}`
            );
          }
          break;

        case 'color':
          if (typeof data.color !== 'string' || data.color === '') {
            throw new WrongDataTypeError(
              `Color should be a not empty string! Check the entered data: ${data.color.toLowerCase()}`
            );
          }
          break;

        case 'vin':
          if (typeof data.vin !== 'number') {
            throw new WrongDataTypeError(`Vin should be a not empty string! Check the entered data: ${data.vin}`);
          }
          break;

        default:
          break;
      }
    });
  }

  addVehicle(newVehicle) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const vehicleExists = this.#vehicles.find((vehicle) => vehicle.vin === newVehicle.vin);

        if (vehicleExists) {
          reject(new EntityFoundError(`Vehicle with ${newVehicle.vin} vin exists!`));
          return;
        }

        if (!(newVehicle instanceof Vehicle)) {
          reject(new IsNotInstanceOfVehicleError(`Vehicle is not an instance of a Truck or Bus`));
          return;
        }

        this.#vehicles.push(newVehicle);
        resolve(`Vehicle with ${newVehicle.vin} vin added successfully!`);
      }, DELAY);
    });
  }

  sellVehicle(vin) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const vehicleExists = this.#vehicles.find((vehicle) => vehicle.vin === vin);

        if (!vehicleExists) {
          reject(new NotFoundError(`Vehicle with ${vin} vin not found`));
          return;
        }
        const vehicleIdx = this.#vehicles.indexOf(vehicleExists);
        this.#vehicles.splice(vehicleIdx, 1);
        resolve(`Truck with VIN=${vin} is sold successfully`);
      }, DELAY);
    });
  }

  findTruck(carryWeight, color) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.dataVerification({ carryWeight, color });

        const foundTruck = this.#vehicles.find(
          (vehicle) => vehicle.carryWeight === carryWeight && vehicle.color === color
        );

        if (!foundTruck) {
          reject(new NotFoundError(`Truck with ${carryWeight} carryWeight and ${color.toLowerCase()} color not found`));
          return;
        }

        resolve(foundTruck);
      }, DELAY);
    });
  }

  paintBus(vin, newColor) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.dataVerification({ vin, color: newColor });

        const foundBus = this.#vehicles.find((vehicle) => vehicle.vin === vin);

        if (!foundBus) {
          reject(new NotFoundError(`Bus with ${vin} vin not found`));
          return;
        }

        if (!(foundBus instanceof Bus)) {
          reject(new IsNotInstanceOfVehicleError(`Vehicle is not an instance of a Bus`));
          return;
        }

        this.#vehicles.map((vehicle) => (vehicle.vin === vin ? (vehicle.color = newColor) : vehicle));
        resolve(`Bus with ${vin} vin painted with ${newColor} color painted successfully!`);
      }, DELAY);
    });
  }

  countVehiclesWithColor(color) {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.dataVerification({ color });
        let counterColors = 0;
        this.#vehicles.forEach((vehicle) => (vehicle.color === color ? (counterColors += 1) : false));
        resolve(counterColors > 0 ? counterColors : 0);
      }, DELAY);
    });
  }
}

class Truck extends Vehicle {
  #carryWeight = 0;

  constructor(vin, color, carryWeight) {
    super(vin, color);
    this.carryWeight = carryWeight;
  }

  set carryWeight(newCarryWeight) {
    this.dataVerification({ carryWeight: newCarryWeight });
    this.#carryWeight = newCarryWeight;
  }

  get carryWeight() {
    return this.#carryWeight;
  }
}

(async () => {
  const dealer = new Dealer(DATABASE.dealer.title);

  console.log(dealer.title);

  const mockTruckOne = new Truck(6730, 'Yellow', 81);

  const mockTruckSecond = new Truck(1896, 'Green', 54);

  const mockTruckThird = new Truck(3356, 'Yellow', 97);

  const mockBusOne = new Bus(8651, 'Black', 97);

  await Promise.allSettled(
    Array(DATABASE.trucks.length)
      .fill(0)
      .map((_, truckIdx) =>
        dealer.addVehicle(
          new Truck(
            DATABASE.trucks[truckIdx].vin,
            DATABASE.trucks[truckIdx].color,
            DATABASE.trucks[truckIdx].carryWeight
          )
        )
      )
  );

  await Promise.allSettled(
    Array(DATABASE.buses.length)
      .fill(0)
      .map((_, busIdx) =>
        dealer.addVehicle(
          new Bus(DATABASE.buses[busIdx].vin, DATABASE.buses[busIdx].color, DATABASE.buses[busIdx].maxPassengers)
        )
      )
  );

  console.table(dealer.vehicles);

  // Chain №1
  await dealer
    .findTruck(10, 'Red')
    .then((truck) => dealer.sellVehicle(truck.vin))
    .then((data) => {
      console.log(data);
      console.table(dealer.vehicles);
    })
    .catch((err) => console.log(err.message));

  // Chain №2
  await dealer
    .countVehiclesWithColor('Yellow')
    .then((quantity) => {
      console.log(`——————————————————————————————————————————`);
      console.log(`Number of vehicle color Yellow — ${quantity}`);
      console.table(dealer.vehicles);
      console.log(`——————————————————————————————————————————`);
    })
    .then(() => dealer.paintBus(6543, 'Green'))
    .then((data) => {
      console.log(data);
      return dealer.countVehiclesWithColor('Yellow');
    })
    .then((quantity) => {
      console.log(`——————————————————————————————————————————`);
      console.log(`Number of vehicle color Yellow — ${quantity}`);
      console.table(dealer.vehicles);
      console.log(`——————————————————————————————————————————`);
    })
    .then(() => dealer.addVehicle(mockTruckOne))
    .then(() => dealer.addVehicle(mockTruckSecond))
    .then(() => dealer.addVehicle(mockTruckThird))
    .then(() => dealer.addVehicle(mockBusOne))
    .then(() => dealer.countVehiclesWithColor('Yellow'))
    .then((quantity) => {
      console.log(`——————————————————————————————————————————`);
      console.log(`Number of vehicle color Yellow — ${quantity}`);
      console.table(dealer.vehicles);
      console.log(`——————————————————————————————————————————`);
    })
    .catch((err) => console.log(err.message));
})();
